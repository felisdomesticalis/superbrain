$( () => {

	var navigation = $('header nav');

	console.log('ready');

	var viewportWidth = $(window).width();

	// on resize
	$(window).on('resize', function(){
    viewportWidth = $(this).width();
	});

	// Hide and show nav on scroll
	function scrollFunction(){
		if ( navigation.hasClass( "scrollToAppear" ) ) {
			let navbarHeightScroll = 75;
			var sn = document.getElementById("navbar");
			if(document.body.scrollTop > 100 || document.documentElement.scrollTop > 100){
				sn.style.top = "0"
			}
			else{
				sn.style.top = "-" + navbarHeightScroll + "px"
			}
		}
	}

	// Update nav selected
	function updateNavigation() {
		var lastId,
		topMenu = $(".nav-list"),
		topMenuHeight = topMenu.outerHeight()+30,
		// All list items
		menuItems = topMenu.find("a"),
		// Anchors corresponding to menu items
		scrollItems = menuItems.map(function(){
			var item = $($(this).attr("href"));
			if (item.length) { return item; }
		});

		// Get container scroll position
		var fromTop = $(this).scrollTop()+topMenuHeight;

		// Get id of current scroll item
		var cur = scrollItems.map(function(){
			if ($(this).offset().top < fromTop)
			return this;
		});
		// Get the id of the current element
		cur = cur[cur.length-1];
		var id = cur && cur.length ? cur[0].id : "";

		if (lastId !== id) {
			lastId = id;
			// Set/remove active class
			menuItems
			.parent().removeClass("active")
			.end().filter("[href='#"+id+"']").parent().addClass("active");
		}
	}

	//On Scroll Functionality
	$(window).scroll( () => {
		var windowTop = $(window).scrollTop();
		// windowTop > 100 ? $('nav').addClass('navShadow') : $('nav').removeClass('navShadow');
		// if ( viewportWidth <= 992 ){
		// 	windowTop > 100 ? $('ul').css('top','60px') : $('ul').css('top','80px');
		// } else {
		// 	windowTop > 100 ? $('ul').css('top','75px') : $('ul').css('top','100px');
		// }
		scrollFunction();
		updateNavigation();
	});




	//Click Logo To Scroll To Top
	$('#logo').on('click', () => {
		$('html,body').animate({
			scrollTop: 0
		},500);
	});

	//Smooth Scrolling Using Navigation Menu
	// $('a[href*="#"]', navigation).on('click', function(e){
	$('a[href*="#"]').on('click', function(e){
		$('li', navigation).removeClass('active');
		$('html,body').animate({
			scrollTop: $($(this).attr('href')).offset().top - 70
		},500);
		$(this).parent().addClass('active');
		e.preventDefault();
	});

	//Toggle Menu
	$('#menu-toggle', navigation).on('click', () => {
		$('#menu-toggle', navigation).toggleClass('closeMenu');
		$('ul', navigation).toggleClass('showMenu');

		$('li', navigation).on('click', () => {
			$('ul', navigation).removeClass('showMenu');
			$('#menu-toggle', navigation).removeClass('closeMenu');
		});
	});

	// CLOSE INFO
	$('#infosection-close').on('click', function(e){
		$(this).parent().fadeOut();
	});

	// HEART ANIMATION
	$(".heart").on("click", function() {
    $(this).toggleClass("is-active");
  });


	// Initialize Tooltips
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})


	// CUSTOM LEAFLET SCRIPT -----------------------------------------------------
	// https://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/

	var map = L.map( 'leafletmap', {
	  // BOCHUM
	  // https://www.latlong.net/place/bochum-north-rhine-westphalia-germany-16285.html
	  center: [51.341492, 7.042751],
	  minZoom: 7,
	  maxZoom: 18,
	  zoom: 15
	});

	var OpenStreetMap_DE = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
	  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	  subdomains: ['a','b','c']
	});

	OpenStreetMap_DE.addTo( map );


	// Define Markers
	markers = [
	   {
	     "name": "Superbrain: Friedrich Straße 107, 42551 Velbert",
	     "url": "",
	     "lat": 51.341492,
	     "lng": 7.042751
	   }
	];

	var pathToImage = './assets/img/'

	var myIcon = L.icon({
	    iconUrl: pathToImage + '/pin.svg',
	    iconRetinaUrl: pathToImage + '/pin.svg',
	    iconSize: [80, 65],
	    iconAnchor: [35, 65],
	    popupAnchor: [0, -40]
	});

	// Add Markers
	for ( var i=0; i < markers.length; ++i ) {
		L.marker( [markers[i].lat, markers[i].lng], {icon: myIcon} )
		 .bindPopup( '<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>' )
		 .addTo( map );
	}


});
